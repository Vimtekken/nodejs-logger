"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chalk_1 = __importDefault(require("chalk"));
const vimtekken_environment_1 = __importDefault(require("vimtekken-environment"));
const loglevel_message_prefix_1 = __importDefault(require("@natlibfi/loglevel-message-prefix"));
const loglevel_1 = __importDefault(require("loglevel"));
const debugFlags = vimtekken_environment_1.default.getEnv('DEBUG_FLAGS', String, 'gui');
const debugExpression = vimtekken_environment_1.default.getEnv('DEBUG', String, '');
const logLevel = vimtekken_environment_1.default.getEnv('LOG_LEVEL', String, 'info');
const shouldProcessDebug = !['debug', 'trace'].includes(logLevel);
class Logger {
    constructor(...names) {
        this.names = names;
        this.debugLog = Logger.createLogger(names, 'debug', chalk_1.default.blue);
        this.errorLog = Logger.createLogger(names, 'error', chalk_1.default.red);
        this.infoLog = Logger.createLogger(names, 'info', chalk_1.default.green);
        this.traceLog = Logger.createLogger(names, 'trace', chalk_1.default.white);
        this.warnLog = Logger.createLogger(names, 'warn', chalk_1.default.yellow);
        this.setLevel(logLevel);
        if (shouldProcessDebug && debugFlags) {
            const debugRegex = RegExp(debugFlags, debugExpression);
            if (debugRegex.test(names.join('|'))) {
                this.setLevel('debug');
            }
        }
    }
    static createLogger(names, level, colorFunc) {
        const log = loglevel_1.default.getLogger(`${names.join('-')}-${level}`);
        if (log.isInitialized) {
            return log;
        }
        log.isInitialized = true;
        return loglevel_message_prefix_1.default(log, {
            prefixFormat: `${colorFunc('[%p]')}`,
            prefixes: ['timestamp'],
            staticPrefixes: names,
            separator: ' | ',
        });
    }
    debug(...logs) {
        this.debugLog.debug(...logs);
    }
    error(...logs) {
        this.errorLog.error(...logs);
    }
    info(...logs) {
        this.infoLog.info(...logs);
    }
    trace(...logs) {
        this.traceLog.trace(...logs);
    }
    warn(...logs) {
        this.warnLog.warn(...logs);
    }
    setLevel(level) {
        this.debugLog.setLevel(level);
        this.errorLog.setLevel(level);
        this.infoLog.setLevel(level);
        this.traceLog.setLevel(level);
        this.warnLog.setLevel(level);
    }
}
exports.default = Logger;
//# sourceMappingURL=index.js.map