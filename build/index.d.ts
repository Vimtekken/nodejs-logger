import { LogLevelDesc } from 'loglevel';
export default class Logger {
    private debugLog;
    private errorLog;
    private infoLog;
    private traceLog;
    private warnLog;
    names: string[];
    constructor(...names: string[]);
    private static createLogger;
    debug(...logs: any[]): void;
    error(...logs: any[]): void;
    info(...logs: any[]): void;
    trace(...logs: any[]): void;
    warn(...logs: any[]): void;
    setLevel(level: LogLevelDesc): void;
}
//# sourceMappingURL=index.d.ts.map