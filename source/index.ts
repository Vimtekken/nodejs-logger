import Chalk from 'chalk';
import Env from 'vimtekken-environment';
import LogLevelMessagePrefix from '@natlibfi/loglevel-message-prefix';
import LogLevel, { LogLevelDesc } from 'loglevel';

const debugFlags = Env.getEnv('DEBUG_FLAGS', String, 'gui');
const debugExpression = Env.getEnv('DEBUG', String, '');
const logLevel = Env.getEnv('LOG_LEVEL', String, 'info') as LogLevelDesc;
const shouldProcessDebug = !(['debug', 'trace'] as Array<LogLevelDesc>).includes(logLevel);

export default class Logger {
	private debugLog: LogLevel.Logger;

	private errorLog: LogLevel.Logger;

	private infoLog: LogLevel.Logger;

	private traceLog: LogLevel.Logger;

	private warnLog: LogLevel.Logger;

	names: string[];

	constructor(...names: string[]) {
		this.names = names;
		this.debugLog = Logger.createLogger(names, 'debug', Chalk.blue);
		this.errorLog = Logger.createLogger(names, 'error', Chalk.red);
		this.infoLog = Logger.createLogger(names, 'info', Chalk.green);
		this.traceLog = Logger.createLogger(names, 'trace', Chalk.white);
		this.warnLog = Logger.createLogger(names, 'warn', Chalk.yellow);
		this.setLevel(logLevel);
		if (shouldProcessDebug && debugFlags) {
			const debugRegex = RegExp(debugFlags, debugExpression);
			if (debugRegex.test(names.join('|'))) {
				this.setLevel('debug');
			}
		}
	}

	private static createLogger(names: string[], level: string, colorFunc: (s: string) => string): LogLevel.Logger {
		const log = LogLevel.getLogger(`${names.join('-')}-${level}`);
		if ((log as any).isInitialized) {
			return log;
		}
		(log as any).isInitialized = true;
		return LogLevelMessagePrefix(
			log,
			{
				prefixFormat: `${colorFunc('[%p]')}`,
				prefixes: ['timestamp'],
				staticPrefixes: names,
				separator: ' | ',
			},
		);
	}

	debug(...logs: any[]): void {
		this.debugLog.debug(...logs);
	}

	error(...logs: any[]): void {
		this.errorLog.error(...logs);
	}

	info(...logs: any[]): void {
		this.infoLog.info(...logs);
	}

	trace(...logs: any[]): void {
		this.traceLog.trace(...logs);
	}

	warn(...logs: any[]): void {
		this.warnLog.warn(...logs);
	}

	setLevel(level: LogLevelDesc): void {
		this.debugLog.setLevel(level);
		this.errorLog.setLevel(level);
		this.infoLog.setLevel(level);
		this.traceLog.setLevel(level);
		this.warnLog.setLevel(level);
	}
}
