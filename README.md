# NodeJS Logger
This is a personal logger for my projects.

# License
This repo is licensed under the [Apache 2.0 License](https://apache.org/licenses/LICENSE-2.0).
